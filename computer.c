#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include "computer.h"
#undef mips			/* gcc already has a def for mips */
#define GET_OPCODE(x) (x>>26)
#define GET_RS(x) ((x>>21)&0x1F)
#define GET_RT(x) ((x>>16)&0x1F)
#define GET_RD(x) ((x>>11)&0x1F)
#define GET_SHAMT(x) ((x>>6)&0x1F)
#define GET_FUNCT(x) (x&0x3F)
#define GET_IMM(x) (x&0xFFFF)
#define GET_TARGET(x) (x&0x3FFFFFF)
#define NUM_INSTRUCTIONS    18


unsigned int endianSwap(unsigned int);

void PrintInfo (int changedReg, int changedMem);
unsigned int Fetch (int);
void Decode (unsigned int, DecodedInstr*, RegVals*);
int Execute (DecodedInstr*, RegVals*);
int Mem(DecodedInstr*, int, int *);
void RegWrite(DecodedInstr*, int, int *);
void UpdatePC(DecodedInstr*, int);
void PrintInstruction (DecodedInstr*);
unsigned int int_to_int (unsigned int k);
void memoryOutOfBounds (int addr);
void incorrectInstruction (int opcode);

//ADDIU GOOD INSTR!? YEAH RIGHT!

/*Globally accessible Computer variable*/
Computer mips;
RegVals rVals;

//Easy way to go from opcode/funct to name of that opcode/funct

typedef enum{ADDU=0x21,SUBU=0x23,SLL=0x0,SRL=0x02,AND=0x24,OR=0x25,SLT=0x2A,JR=0x08} rTypeInstr;

typedef enum{ADDIU=0x9,ANDI=0x0C,ORI=0x0D,LUI=0x0F,BEQ=0x04,BNE=0x05,LW=0x23,SW=0x2B} iTypeInstr;

typedef enum{JINSTR=0x02,JAL=0x03} jTypeInstr;


/*
 *  Return an initialized computer with the stack pointer set to the
 *  address of the end of data memory, the remaining registers initialized
 *  to zero, and the instructions read from the given file.
 *  The other arguments govern how the program interacts with the user.
 */
void InitComputer (FILE* filein, int printingRegisters, int printingMemory,
  int debugging, int interactive) {
    int k;
    unsigned int instr;

    /* Initialize registers and memory */

    for (k=0; k<32; k++) {
        mips.registers[k] = 0;
    }
    
    /* stack pointer - Initialize to highest address of data segment */
    mips.registers[29] = 0x00400000 + (MAXNUMINSTRS+MAXNUMDATA)*4;

    for (k=0; k<MAXNUMINSTRS+MAXNUMDATA; k++) {
        mips.memory[k] = 0;
    }

    k = 0;
    while (fread(&instr, 4, 1, filein)) {
	/*swap to big endian, convert to host byte order. Ignore this.*/
        mips.memory[k] = ntohl(endianSwap(instr));
        k++;
        if (k>MAXNUMINSTRS) {
            fprintf (stderr, "Program too big.\n");
            exit (1);
        }
    }

    mips.printingRegisters = printingRegisters;
    mips.printingMemory = printingMemory;
    mips.interactive = interactive;
    mips.debugging = debugging;
}

unsigned int endianSwap(unsigned int i) {
    return (i>>24)|(i>>8&0x0000ff00)|(i<<8&0x00ff0000)|(i<<24);
}

/*
 *  Run the simulation.
 */
void Simulate () {
    char s[40];  /* used for handling interactive input */
    unsigned int instr;
    int changedReg=-1, changedMem=-1, val;
    DecodedInstr d;
    
    /* Initialize the PC to the start of the code section */
    mips.pc = 0x00400000;
    while (1) {
        if (mips.interactive) {
            printf ("> ");
            fgets (s,sizeof(s),stdin);
            if (s[0] == 'q') {
                return;
            }
        }

        /* Fetch instr at mips.pc, returning it in instr */
        instr = Fetch (mips.pc);

        printf ("Executing instruction at %8.8x: %8.8x\n", mips.pc, instr);

        /* 
	 * Decode instr, putting decoded instr in d
	 * Note that we reuse the d struct for each instruction.
	 */
        Decode (instr, &d, &rVals);

        /*Print decoded instruction*/
        PrintInstruction(&d);

        /* 
	 * Perform computation needed to execute d, returning computed value 
	 * in val 
	 */
        val = Execute(&d, &rVals);

        UpdatePC(&d,val);

        /* 
	 * Perform memory load or store. Place the
	 * address of any updated memory in *changedMem, 
	 * otherwise put -1 in *changedMem. 
	 * Return any memory value that is read, otherwise return -1.
         */
        val = Mem(&d, val, &changedMem);

        /* 
	 * Write back to register. If the instruction modified a register--
	 * (including jal, which modifies $ra) --
         * put the index of the modified register in *changedReg,
         * otherwise put -1 in *changedReg.
         */
        RegWrite(&d, val, &changedReg);

        PrintInfo (changedReg, changedMem);
    }
}

/*
 *  Print relevant information about the state of the computer.
 *  changedReg is the index of the register changed by the instruction
 *  being simulated, otherwise -1.
 *  changedMem is the address of the memory location changed by the
 *  simulated instruction, otherwise -1.
 *  Previously initialized flags indicate whether to print all the
 *  registers or just the one that changed, and whether to print
 *  all the nonzero memory or just the memory location that changed.
 */
void PrintInfo ( int changedReg, int changedMem) {
    int k, addr;
    printf ("New pc = %8.8x\n", mips.pc);
    if (!mips.printingRegisters && changedReg == -1) {
        printf ("No register was updated.\n");
    } else if (!mips.printingRegisters) {
        printf ("Updated r%2.2d to %8.8x\n",
        changedReg, mips.registers[changedReg]);
    } else {
        for (k=0; k<32; k++) {
            printf ("r%2.2d: %8.8x  ", k, mips.registers[k]);
            if ((k+1)%4 == 0) {
                printf ("\n");
            }
        }
    }
    if (!mips.printingMemory && changedMem == -1) {
        printf ("No memory location was updated.\n");
    } else if (!mips.printingMemory) {
        printf ("Updated memory at address %8.8x to %8.8x\n",
        changedMem, Fetch (changedMem));
    } else {
        printf ("Nonzero memory\n");
        printf ("ADDR	  CONTENTS\n");
        for (addr = 0x00400000+4*MAXNUMINSTRS;
             addr < 0x00400000+4*(MAXNUMINSTRS+MAXNUMDATA);
             addr = addr+4) {
            if (Fetch (addr) != 0) {
                printf ("%8.8x  %8.8x\n", addr, Fetch (addr));
            }
        }
    }
}

/*
 *  Return the contents of memory at the given address. Simulates
 *  instruction fetch. 
 */
unsigned int Fetch ( int addr) {
    return mips.memory[(addr-0x00400000)/4];
}

/* Decode instr, returning decoded instruction. */
void Decode ( unsigned int instr, DecodedInstr* d, RegVals* rVals) {
    //printf("Instruction: 0x%x Opcode: 0x%x\n",instr,GET_OPCODE(instr));
    int op = GET_OPCODE(instr);
    d->op = op;
    if(op)
    {
        //Now check if it is an I or a J type
        if(op == 2 || op == 3)
        {
            //This is a J-type instruction
            d->type = J;

            //Create a pointer to the union in d's struct that will hold the regs
            //That will be of type RRegs and it will modify the memory in d
            JRegs * j = (JRegs *)(&(d->regs));
            j->target = GET_TARGET(instr);
        }
        else
        {
            //This is an I-type Instruction
            d->type = I;

            //Create a pointer to the union in d's struct that will hold the regs
            //That will be of type RRegs and it will modify the memory in d
            IRegs * i = (IRegs *)(&(d->regs));
            i->rs = GET_RS(instr);
            i->rt = GET_RT(instr);
            i->addr_or_immed = GET_IMM(instr);
            //This number needs to be sign extended with 1s if there is a 1 in the 16th bit
            if ((i->addr_or_immed&0x8000) != 0)
                i->addr_or_immed |= 0xFFFF0000;

            //Populating rVals with the register values
            rVals->R_rs = mips.registers[i->rs];
            rVals->R_rt = mips.registers[i->rt];

            //printf ("Setting imm to %x\n", i->addr_or_immed);
            //printf ("Setting rs to %x, settings rt to %x\n", rVals->R_rs, rVals->R_rt);
        }
    }
    else
    {
        //This is an R-type instruction
        d->type = R;

        //Create a pointer to the union in d's struct that will hold the regs
        //That will be of type RRegs and it will modify the memory in d
        RRegs * r = (RRegs *)(&(d->regs));
        r->rs = GET_RS(instr);
        r->rt = GET_RT(instr);
        r->rd = GET_RD(instr);
        r->shamt = GET_SHAMT(instr);
        r->funct = GET_FUNCT(instr);

        //Populating rVals with the register values
        rVals->R_rs = mips.registers[r->rs];
        rVals->R_rt = mips.registers[r->rt];
        rVals->R_rd = mips.registers[r->rd];
    }

}

/*
 *  Print the disassembled version of the given instruction
 *  followed by a newline.
 */
void PrintInstruction ( DecodedInstr* d) {
    /* Your code goes here */
    if(d->type == R)
    {
        switch (d->regs.r.funct) {
            case ADDU:
                printf("addu\t$%d, $%d, $%d\n",d->regs.r.rd,d->regs.r.rs,d->regs.r.rt);
                break;
            case SUBU:
                printf("subu\t$%d, $%d, $%d\n",d->regs.r.rd,d->regs.r.rs,d->regs.r.rt);
                break;
            case AND:
                printf("and\t$%d, $%d, $%d\n",d->regs.r.rd,d->regs.r.rs,d->regs.r.rt);
                break;
            case OR:
                printf("or\t$%d, $%d, $%d\n",d->regs.r.rd,d->regs.r.rs,d->regs.r.rt);
                break;
            case SLT:
                printf("slt\t$%d, $%d, $%d\n",d->regs.r.rd,d->regs.r.rs,d->regs.r.rt);
                break;
            case SRL:
                printf("srl\t$%d, $%d, $%d\n",d->regs.r.rd,d->regs.r.rt,d->regs.r.shamt);
                break;
            case SLL:
                printf("sll\t$%d, $%d, $%d\n",d->regs.r.rd,d->regs.r.rt,d->regs.r.shamt);
                break;
            case JR:
                printf("jr\t$%d\n",d->regs.r.rs);
                break;
        }
    }
    else if(d->type == J)
    {
        switch (d->op) {
            case JINSTR:
                printf("j\t0x%8.8x\n",(mips.pc & 0xF0000000) | (d->regs.j.target << 2));
                 break;
            case JAL:
                printf("jal\t0x%8.8x\n",(mips.pc & 0xF0000000) | (d->regs.j.target << 2));
        }
    }
    else if(d->type == I)
    {
        switch (d->op) {
            case ADDIU:
                printf("addiu\t$%d, $%d, %d\n",d->regs.i.rt,d->regs.i.rs,d->regs.i.addr_or_immed);
                break;
            case LW:
                printf("lw\t$%d, %d($%d)\n",d->regs.i.rt,d->regs.i.addr_or_immed,d->regs.i.rs);
                break;
            case SW:
                printf("sw\t$%d, %d($%d)\n",d->regs.i.rt,d->regs.i.addr_or_immed,d->regs.i.rs);
            case LUI:
                printf("lui\t$%d, 0x%x\n",d->regs.i.rt,d->regs.i.addr_or_immed);
                break;
            case ANDI:
                printf("andi\t$%d, $%d, 0x%x\n",d->regs.i.rt,d->regs.i.rs,d->regs.i.addr_or_immed);
                break;
            case ORI:
                printf("ori\t$%d, $%d, 0x%x\n",d->regs.i.rt,d->regs.i.rs,d->regs.i.addr_or_immed);
                break;
            case BEQ:
                printf("beq\t$%d, $%d, 0x%8.8x\n",d->regs.i.rs,d->regs.i.rt,4*d->regs.i.addr_or_immed+mips.pc+4);
                break;
            case BNE:
                printf("bne\t$%d, $%d, 0x%8.8x\n",d->regs.i.rs,d->regs.i.rt,4*d->regs.i.addr_or_immed+mips.pc+4);
                break;


        }
    }
}

/* Perform computation needed to execute d, returning computed value */
int Execute ( DecodedInstr* d, RegVals* rVals) {
    // Initialize value to write to register
    int val = 0;

    // Break up by instruction type
    // Read/modify register/mem values based on opcode
    if (d->type == I)
    {
        switch (d->op) {
            // addiu: rt = rs + i
            // Type: I
            // Calculate the sum of a register and an immediate value
            case ADDIU:
                val = rVals->R_rs + d->regs.i.addr_or_immed;
                break;

            // andi: rt = rs & imm
            // Type: I
            // Bitwise AND a register and an immediate
            case ANDI:
                val = rVals->R_rs & d->regs.i.addr_or_immed;
                break;

            // ori: rt = rs | imm
            // Type: I
            // Bitwise OR a register and an immediate
            case ORI:
                val = rVals->R_rs | rVals->R_rt;
                break;

            // lui: rt = (imm << 16)
            // Type: I
            // Shift imm 16 bit left and store in register. Zeros are shifted in
            case LUI:
                val = rVals->R_rt << 16;
                break;

            // beq: if rs == rt: advance_pc (offset << 2);
            // Type: I
            // Branch if registers values are equal
            case BEQ:
                val = (rVals->R_rs == rVals->R_rt);
                break;

            // bne: if rs != rt: advance_pc (offset << 2)
            // Type: I
            // Branch if registers values are not equal
            case BNE:
                val = (rVals->R_rs != rVals->R_rt);
                break;

            // lw: rt = mem[rs + offset]
            // Type: I
            // Load a word into a register from an address
            case LW:
                val = rVals->R_rs + d->regs.i.addr_or_immed;
                break;

            // sw: mem[rs + offset] = rt
            // Type: I
            // Store the contents of a register into a memory address
            case SW:
                val = rVals->R_rs + d->regs.i.addr_or_immed;
                break;

            default:
                //printf ("Opcode not found in I type!\n");
                incorrectInstruction (d->op);
                break;
        }
    } 
    else if (d->type == R)
    {
        switch (d->regs.r.funct) {
            // slt: if rs < rt: rd = 1; else rd = 0
            // Type: R
            // Set register value based on register comparison
            case SLT:
                val = (rVals->R_rs < rVals->R_rt);
                break;

            // or: rd = rs | rt
            // Type: R
            // Bitwise OR two registers
            case OR:
                val = (rVals->R_rs | rVals->R_rt);
                break;

            // addu: rd = rs + rt
            // Type: R
            // Calculate the sum of two registers
            case ADDU:
                val = rVals->R_rs + rVals->R_rt;
                break;

            // subu: rd = rs - rt
            // Type: R
            // Calculate the difference between two registers
            case SUBU:
                val = rVals->R_rs - rVals->R_rt;
                break;

            // sll: rd = rt << rs
            // Type: R
            // Shift rd left by the shift amount (rs). Zeros are shifted in
            case SLL:
                val = rVals->R_rt << d->regs.r.shamt;
                break;

            // srl: rd = rt >> rs
            // Type: R
            // Shift rd right by the shift amount (rs). Zeros are shifted in
            case SRL:
                val = rVals->R_rt >> d->regs.r.shamt;
                break;

            // and: rd = rs & rt
            // Type: R
            // Bitwise AND two registers
            case AND:
                val = rVals->R_rs & rVals->R_rt;
                break;

            // jr: PC = rs
            // Type: R
            // Jump to address stored in rs
            case JR:
                val = rVals->R_rs;
                break;

            default:
                //printf ("Opcode not found in R type!\n");
                incorrectInstruction (d->regs.r.funct);
                break;
        }
    } 
    else if (d->type == J)
    {
        switch (d->op) {

            // j: PC = (PC & 0xf0000000) | (target << 2)
            // Type: J
            // Jumps to the calculated address
            case JINSTR:
                val = (mips.pc & 0xf0000000) | (d->regs.j.target << 2);
                break;

            // jal: r31 = ( (PC & 0xf0000000) | (target << 2) ) + 4
            // Type: J
            // Jumps to the calculated address and store result in reg31
            case JAL:
                val = mips.pc + 4;
                break;

            default:
                //printf ("Opcode not found in J type!\n");
                incorrectInstruction (d->op);
                break;
        }
    } else {
        //printf ("DEBUG: Incorrect d type received!");
        incorrectInstruction (d->type);
        return 0;
    }

    //printf("EXEC returning value: %d\n",val);
    return val;
}

/* 
 * Update the program counter based on the current instruction. For
 * instructions other than branches and jumps, for example, the PC
 * increments by 4 (which we have provided).
 */
void UpdatePC ( DecodedInstr* d, int val) {
    // 1 if we're processing a branch
    int branch = 0;

    // Determine instruction type
    switch (d->type) {
        // Jump if jump instruction
        case J:
            val = (mips.pc & 0xf0000000) | (d->regs.j.target << 2);
            //printf ("Incrementing by jump value\n");
            mips.pc = val;
            break;
        case R:
            if(d->regs.r.funct == JR)
            {
                //printf("JR to register %d\n",d->regs.r.rs);
                mips.pc = mips.registers[d->regs.r.rs];
            }
            else
            {
                mips.pc += 4;
            }
            break;
        case I:
            printf("");
            int rs = mips.registers[d->regs.i.rs];
            int rt = mips.registers[d->regs.i.rt];

            if (d->op == BEQ)
            {
                if(rs == rt)
                {
                    mips.pc += 4*d->regs.i.addr_or_immed + 4;
                }
                else
                {
                    mips.pc += 4;
                }
                //printf("Branch dis: %d\n",branch);
                break;
            }
            else if (d->op == BNE) // bne
            {
                if(rs != rt)
                {
                    mips.pc += 4*d->regs.i.addr_or_immed + 4;
                }
                else
                {
                    mips.pc += 4;
                }
                //printf("not Branch dis: %d\n",branch);
                break;
            } 
            else 
            {
                //printf ("Incrementing by branch 4\n");
                mips.pc += 0x4;
            }
            break;
        default:
            // Just increment by val if not jump/branch
            //printf ("Incrementing by 4\n");
            mips.pc += 0x4;
    }
}

/*
 * Perform memory load or store. Place the address of any updated memory 
 * in *changedMem, otherwise put -1 in *changedMem. Return any memory value 
 * that is read, otherwise return -1. 
 *
 * Remember that we're mapping MIPS addresses to indices in the mips.memory 
 * array. mips.memory[0] corresponds with address 0x00400000, mips.memory[1] 
 * with address 0x00400004, and so forth.
 *
 */
int Mem( DecodedInstr* d, int val, int *changedMem) {
    *changedMem = -1;
    if(d->type == I && (d->op == SW || d->op == LW))
    {
        if (val >= 0x00400000 && val <= 0x00404000)
        {
            //This is the index in our mips.memory array
            int memIndex = (val-0x00400000)>>2;

            if(val & 3)
            {
                //This means the value is not word aligned and we should throw an error
                //TODO: throw error
                printf ("Memory Access Exception at 0x%x: address 0x%x\n",mips.pc-4,val);
                exit(0);
            }

            if(d->op == SW)
            {
                //store word instruction
                
                //Store the address of memory being changed
                *changedMem = val;


                //Make the address contain the value of the rt register
                IRegs * iReg = (IRegs *)(&d->regs);
                int regIndex = iReg->rt;
                mips.memory[memIndex]  = mips.registers[regIndex];

                return -1;
            }
            else if(d->op == LW)
            {
                //Load word instruction
                *changedMem = -1;
                return mips.memory[memIndex];
            }
        }
    }

  return val;
}

/* 
 * Write back to register. If the instruction modified a register--
 * (including jal, which modifies $ra) --
 * put the index of the modified register in *changedReg,
 * otherwise put -1 in *changedReg.
 */
void RegWrite( DecodedInstr* d, int val, int *changedReg) {
    // -1 if unchanged
    int * initChangedReg = changedReg;
    changedReg = (int*) -1;
    mips.registers[0] = 0;
    //printf ("Made it to RegWrite where val is: %d\n",val);

    // If register was changed, say which one in changedReg
    if (d->type == I)
    {
        switch (d->op) {
            // addiu: rt = rs + i
            // Type: I
            // Calculate the sum of a register and an immediate value
            case ADDIU:
                changedReg = (int*) d->regs.i.rt;
                break;

            // andi: rt = rs & imm
            // Type: I
            // Bitwise AND a register and an immediate
            case ANDI:
                changedReg = (int*) d->regs.i.rt;
                break;

            // ori: rt = rs | imm
            // Type: I
            // Bitwise OR a register and an immediate
            case ORI:
                changedReg = (int*) d->regs.i.rt;
                break;

            // lui: rt = (imm << 16)
            // Type: I
            // Shift imm 16 bit left and store in register. Zeros are shifted in
            case LUI:
                changedReg = (int*) d->regs.i.rt;
                break;

            // beq: if rs == rt: advance_pc (offset << 2);
            // Type: I
            // Branch if registers values are equal
            case BEQ:
                break;

            // bne: if rs != rt: advance_pc (offset << 2)
            // Type: I
            // Branch if registers values are not equal
            case BNE:
                break;

            // lw: rt = mem[rs + offset]
            // Type: I
            // Load a word into a register from an address
            case LW:
                changedReg = (int*) d->regs.i.rt;
                break;

            // sw: mem[rs + offset] = rt
            // Type: I
            // Store the contents of a register into a memory address
            case SW:
                changedReg = -1;
                break;

            default:
                //printf ("Opcode not found in I type!\n");
                incorrectInstruction (d->op);
                break;
        }
    } 
    else if (d->type == R)
    {
        switch (d->regs.r.funct) {
            // slt: if rs < rt: rd = 1; else rd = 0
            // Type: R
            // Set register value based on register comparison
            case SLT:
                changedReg = (int*) d->regs.r.rd;
                break;

            // or: rd = rs | rt
            // Type: R
            // Bitwise OR two registers
            case OR:
                changedReg = (int*) d->regs.r.rd;
                break;

            // addu: rd = rs + rt
            // Type: R
            // Calculate the sum of two registers
            case ADDU:
                changedReg = (int*) d->regs.r.rd;
                break;

            // subu: rd = rs - rt
            // Type: R
            // Calculate the difference between two registers
            case SUBU:
                changedReg = (int*) d->regs.r.rd;
                break;

            // sll: rd = rt << rs
            // Type: R
            // Shift rd left by the shift amount (rs). Zeros are shifted in
            case SLL:
                changedReg = (int*) d->regs.r.rd;
                break;

            // srl: rd = rt >> rs
            // Type: R
            // Shift rd right by the shift amount (rs). Zeros are shifted in
            case SRL:
                changedReg = (int*) d->regs.r.rd;
                break;

            // and: rd = rs & rt
            // Type: R
            // Bitwise AND two registers
            case AND:
                changedReg = (int*) d->regs.r.rd;
                break;

            // jr: PC = rs
            // Type: R
            // Jump to address stored in rs
            case JR:
                break;

            default:
                //printf ("Opcode not found in R type!\n");
                incorrectInstruction (d->regs.r.funct);
                break;
        }
    } 
    else if (d->type == J)
    {
        switch (d->op) {

            // j: PC = (PC & 0xf0000000) | (target << 2)
            // Type: J
            // Jumps to the calculated address
            case JINSTR:
                break;

            // jal: r31 = ( (PC & 0xf0000000) | (target << 2) ) + 4
            // Type: J
            // Jumps to the calculated address and store result in reg31
            case JAL:
                changedReg = (int*) 31;
                break;

            default:
                //printf ("Opcode not found in J type!\n");
                incorrectInstruction (d->op);
                break;
        }
    } else {
        //printf ("DEBUG: Incorrect d type received!");
        incorrectInstruction (d->type);
    }

    if (changedReg != -1)
        // Set register back to original value
        //printf("Changed register: %d from %d to %d\n",(int)changedReg,mips.registers[(int)changedReg],val);
        mips.registers[(int) changedReg] = val;
        *initChangedReg = (int)changedReg;
}

// Exits and prints incorrect instruction message
void memoryOutOfBounds (int addr) {
    printf ("Memory Access Exception at %d: 0x%x\n", mips.pc, addr);
    exit(0);
}

void incorrectInstruction (int opcode) {
    printf ("Incorrect Instruction found with op code: %x", opcode);
    exit(0);
}
