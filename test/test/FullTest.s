# Short test case for your project.
#
# Note that this is by no means a comprehensive st!
		.text
		addiu	$t0,$0,0
		addiu	$a0,$0,5
SWLoop:
		addiu	$t0,$t0,1
		sw	$t0,0($sp)
		addiu	$sp,$sp,-4
		bne	$t0,$a0,SWLoop
		
		lw	$t1,4($sp)
		
OtherInstr:	
		addu	$t3,$0,$t1
		addiu	$t3,$t3,3
		subu	$t3,$t3,3
		sll	$t3,$t3,1
		srl	$t3,$t3,1
		and	$t4,$t3,$0
		andi	$t3,$t3,3
		jal	Done
		j	SrslyDone
		
Done:	
		jr		$ra
		
SrslyDone: